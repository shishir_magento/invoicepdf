<?php
/**
 * Xicom techno.
 *
 * @category  Xicom 
 * @package   Xicom_InvoicePdf
 * @author    Xicom (Shihsir Chaudhary)
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Xicom_InvoicePdf',
    __DIR__
);
