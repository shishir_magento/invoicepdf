<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/**
 * Xicom techno.
 *
 * @category  Xicom 
 * @package   Xicom_InvoicePdf
 * @author    Xicom (Shihsir Chaudhary)
 * Block of links in Order view page
 */
namespace Xicom\InvoicePdf\Block;


/**
 * @api
 * @since 100.0.2
 */
class Invoicedownload extends \Magento\Sales\Block\Order\Info\Buttons
{
    public function getInvoiceDetails(){
        $order_id = $this->getRequest()->getParam('order_id');
        $orderdetails = $this->getOrder();
        
        foreach ($orderdetails->getInvoiceCollection() as $invoice)
            {
                $invoice_id = $invoice->getId();
            }
        return $invoice_id;
    }
}
